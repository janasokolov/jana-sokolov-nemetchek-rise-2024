# Nemetschek Bulgaria RISE 2024 Homework

### name
Jana Sokolov

### email
sokolov.jana1@gmail.com 

### In the master branch, we can see the code.

### *homework.js*
First we are loading data from JSON file and parsing data.

*`function dijkstra`: 
The Dijkstra algorithm is a graph search algorithm that finds the shortest path between nodes in a graph, which may represent, for example, road networks or computer networks. In the provided code, the Dijkstra algorithm is used to calculate distances between nodes in a graph.

After dijkstra function implementation, we are doing conversion of data from JSON format to a graph.

*`function calculate distances`: calculates the distance between two points represented by their coordinates and is used for adding distances between all nodes in the graph

*`function estimateDeliveryTime`: alculates the maximum time needed for delivery in a given graph based on the provided drone speed. When using the function estimateDeliveryTime, we utilize a helper function called dijkstra. 

*`function calculateRequiredDrones`: First, we calculate the total energy consumption for all orders. Then, we calculate the number of required drones based on battery capacity.

### *addOrder.js*
Geting form input values from HTML, creating an order object and then append order object to JSON file using funciton
*`function appendOrderToJson(newOrder)`
